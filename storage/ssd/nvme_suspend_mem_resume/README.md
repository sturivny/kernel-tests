# storage/ssd/nvme_suspend_mem_resume

Storage: Test suspend to mem and resume on nvme server

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
####  You need run test_dev_setup to define TEST_DEVS
```bash
bash ./runtest.sh
```
